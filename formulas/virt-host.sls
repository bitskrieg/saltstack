include:
  - /states/disk/virt-prepare
  - /states/virt/image-config
  - /states/net/bridge-install
  - /states/virt/libvirt-install
  - /states/net/bridge-config
  - /states/virt/libvirt-config
