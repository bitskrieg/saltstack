authorized_keys:
  file.managed:
    - source: salt://apps/sshd/authorized_keys
    - name: /root/.ssh/authorized_keys
    - makedirs: True
