include:
  - /states/net/bridge-install

ens2f0:
  network.managed:
    - enabled: True
    - type: eth
    - bridge: br0
    - proto: manual
    - require:
      - sls: /states/net/bridge-install

br0:
  network.managed:
    - enabled: True
    - type: bridge
    - proto: dhcp
    - ports: ens2f0
    - require:
      - network: ens2f0
