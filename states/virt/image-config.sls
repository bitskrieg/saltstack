include:
  - /states/disk/virt-prepare

/kvmfs/images:
  file.directory:
    - makedirs: True

{% import_text 'files/image_data' as tuples %}
{% set lines = tuples.split('\n') %}

{% for item in lines %}
/kvmfs/images/{{ item.split(';')[0] }}:
  file.managed:
    - source: {{ item.split(';')[1] }}
    - source_hash: {{ item.split(';')[2] }}
    - require:
      - /kvmfs/images
      - sls: /states/disk/virt-prepare
{% endfor %}
