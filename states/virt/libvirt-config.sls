include:
  - /states/virt/libvirt-install

libvirtd:
  service.running:
    - name: libvirtd
    - require:
      - sls: /states/virt/libvirt-install
